/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edutech;

/**
 *
 * @author Alberto
 */
public class Pregunta {
    private int idPregunta;
    private String pregunta;
    private String respuesta;
    private int puntosSuma;
    private int puntosResta;
    private boolean correcta;

    public Pregunta(int idPregunta, String pregunta, int puntosSuma, int puntosResta) {
        this.idPregunta = idPregunta;
        this.pregunta = pregunta;
        this.puntosSuma = puntosSuma;
        this.puntosResta = puntosResta;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public boolean isCorrecta() {
        return correcta;
    }

    public void setCorrecta(boolean correcta) {
        this.correcta = correcta;
    }

    public int getPuntosSuma() {
        return puntosSuma;
    }

    public void setPuntosSuma(int puntosSuma) {
        this.puntosSuma = puntosSuma;
    }

    public int getPuntosResta() {
        return puntosResta;
    }

    public void setPuntosResta(int puntosResta) {
        this.puntosResta = puntosResta;
    }
 
}
