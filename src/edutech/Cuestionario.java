/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edutech;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alberto
 */
public class Cuestionario {
    private int idCuestionario;
    private String tema;
    private String descripcion;
    private List<Pregunta> preguntas = new ArrayList ();

    public Cuestionario() {
    }
    
    public Cuestionario(int idCuestionario, String tema, String descripcion) {
        this.idCuestionario = idCuestionario;
        this.tema = tema;
        this.descripcion = descripcion;
    }

    public int getIdCuestionario() {
        return idCuestionario;
    }

    public void setIdCuestionario(int idCuestionario) {
        this.idCuestionario = idCuestionario;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Pregunta> getPreguntas() {
        return preguntas;
    }

    public Pregunta getPregunta(int puntero) {
        return preguntas.get(puntero);
    }
    
    public void setPreguntas(List<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }
   
    public void agregaPregunta(Pregunta pregunta) { //Agrega una pregunta al cuestionario.
        preguntas.add(pregunta);
    }
    
    public void eliminaPregunta(Pregunta pregunta) { //Elimina una pregunta del cuestionario.
        preguntas.remove(pregunta);
    }
    
    public void modificaPregunta(int indice, Pregunta pregunta) {
        preguntas.add(indice, pregunta);
    }
}
