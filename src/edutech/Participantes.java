/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edutech;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alberto
 */
public class Participantes {

    List<Grupo> grupos = new ArrayList();

    public Participantes() {
    }

    public List<Grupo> getGrupos() { //Devuelve los grupos inscriptos.
        return grupos;
    }

    public Grupo getGrupo(int i) { //Devuelve un grupo.
        return grupos.get(i);
    }
    
    
    public void agregaGrupo(Grupo grupo) { //Agrega un grupo.
        grupos.add(grupo);
    }

    public void eliminaGrupo(Grupo grupo) { //Elimina un grupo.
        grupos.remove(grupo);
    }

}
