/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edutechui;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortPacketListener;

import edutech.*;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortPacketListener;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.scene.shape.Circle;

/**
 *
 * @author Alberto
 */
public class FXMLDocumentController implements Initializable, SerialPortPacketListener {

    boolean finalizo; // Habiliata la escucha si no llegó el final de la partida.
    boolean atiende_grupo;// Verifica si presiono un grupo 
    private int puntero; //Marca la pregunta a mostrar o modificar.
    private Cuestionario cuestionario;
    private Participantes participantes;
    private int grupo_presiono; //Guarda temporalmente el grupo que presiono para responder.
    private int grupo_ganador;//Guarda el numero del equipo ganador.
    private int empate; //Guarda 1 para empate entre Grupo 1 y 2, guarda 2 para empate entre 2 y 3, 3 para empate entre 1 y 3 y 4 para triple empate.
    private List<Circle> circulos;
    private int ancho_pantalla;
    private int alto_pantalla;

    @FXML
    private TextField textfield_numero_pregunta1;
    @FXML
    private TextField textfield_punto_suma;
    @FXML
    private TextField textfield_punto_resta;
    @FXML
    private Label label_numero_pregunta;
    @FXML
    private Label label_pizarron_verde;
    @FXML
    private Label label_punto_suma;
    @FXML
    private Label label_punto_resta;
    @FXML
    private Label label_cont1;
    @FXML
    private Label label_cont2;
    @FXML
    private Label label_cont3;
    @FXML
    private Label label_ganador;
    @FXML
    private TextField textfield_pizarron_gris;
    @FXML
    private Pane pane_cuestionario;
    @FXML
    private Pane pane_comenzar;
    @FXML
    private Pane pane_ganador;
    @FXML
    private TextArea text;
    @FXML
    private Button btn_nuevo_cuestionario;
    @FXML
    private Button btn_nuevo_grupo;
    @FXML
    private Button btn_comenzar;
    @FXML
    private Button btn_salir;
    @FXML
    private ImageView imagen_fondo;
    @FXML
    private ImageView imagen_cuestionario;
    @FXML
    private ImageView imagen_comenzar;
    @FXML
    private ImageView imagen_ganador;
    @FXML
    private Circle circle1;
    @FXML
    private Circle circle2;
    @FXML
    private Circle circle3;

    public FXMLDocumentController() {

        cuestionario = new Cuestionario(); // Inicializa el objeto cuestionario.
        puntero = 0; // Inicializa el puntero de cuestionario.
        atiende_grupo = false; // Inicializa la variable para que NO lea el boton.
        finalizo = true; // Deshabiliata la escucha si no llegó el final de la partida.
        grupo_presiono = 0; //Inicializa temporalmente el grupo que presiono para responder.
        grupo_ganador = 0;//Inicializa el numero del equipo ganador.
        empate = 0; //Inicializa estado de partida.
        circulos = new ArrayList<Circle>(); //ArrayList con los circulos de los competidores.
        circulos.add(circle1);
        circulos.add(circle2);
        circulos.add(circle3);

        // Ajuste de tamaño de objetos a la resolución de pantalla.
        Toolkit t = Toolkit.getDefaultToolkit(); // instancia a la clase Toolkit. Utilizadapara obtener resolucion de pantalla
        Dimension screenSize = t.getScreenSize(); //Obtiene del método las dimensiones de la pantalla.

        ancho_pantalla = screenSize.width; //Obtiene el ancho y lo asigna a ancho_pantalla.
        alto_pantalla = screenSize.height; //Obtiene el alto y lo asigna a alto_pantalla.

        /*
         *Dimensiona los objetos en pantalla según resolución.
         *@ancho_pantalla: ancho resolución.
         *@alto_pantalla: alto resolución.
         */
 /*
        pane_comenzar.setPrefWidth(ancho_pantalla);
        pane_comenzar.setPrefHeight(alto_pantalla);
        
        pane_cuestionario.setPrefWidth(ancho_pantalla);
        pane_cuestionario.setPrefHeight(alto_pantalla);
        
        pane_cuestionario.setPrefWidth(ancho_pantalla/4);
        pane_cuestionario.setPrefHeight(alto_pantalla/4);
        
        imagen_fondo.setFitWidth(ancho_pantalla);
        imagen_fondo.setFitHeight(alto_pantalla);
        
        imagen_cuestionario.setFitWidth(ancho_pantalla);
        imagen_cuestionario.setFitHeight(alto_pantalla);
        
        imagen_comenzar.setFitWidth(ancho_pantalla);
        imagen_comenzar.setFitHeight(alto_pantalla);
        
        imagen_ganador.setFitWidth(ancho_pantalla/4);
        imagen_ganador.setFitHeight(alto_pantalla/4);
        
        btn_nuevo_cuestionario.setPrefWidth(ancho_pantalla/8);
        btn_nuevo_cuestionario.setPrefHeight(alto_pantalla/6);
        
        btn_nuevo_grupo.setPrefWidth(ancho_pantalla/8);
        btn_nuevo_grupo.setPrefHeight(alto_pantalla/6);
        
        btn_comenzar.setPrefWidth(ancho_pantalla/8);
        btn_comenzar.setPrefHeight(alto_pantalla/6);
        
        btn_salir.setPrefWidth(ancho_pantalla/8);
        btn_salir.setPrefHeight(alto_pantalla/6);*/
        //Codigo temporal hasta definir el origen del cuestionario cargado y la forma de persistencia.
        // Se cargan las preguntas en objetos pregunta.
        Pregunta preg01 = new Pregunta(1, "Cómo se llama nuestra estrella", 5, 2);
        Pregunta preg02 = new Pregunta(2, "Cuantos planetas se conocen en el sistema planetario", 2, 1);
        Pregunta preg03 = new Pregunta(3, "Cual es el nombre de nuestro planeta", 2, 1);
        Pregunta preg04 = new Pregunta(4, "En qué orden se encuentra nuestro planeta", 5, 2);
        Pregunta preg05 = new Pregunta(5, "Cuál es el planeta más grande del sistema", 7, 4);
        Pregunta preg06 = new Pregunta(6, "Cual es el nombre de los dos planetas más cercanos al nuestro", 7, 4);
        Pregunta preg07 = new Pregunta(7, "Cómo se llama nuestro satélite natural", 2, 1);
        Pregunta preg08 = new Pregunta(8, "A qué distancia está nuestro planeta de nuestra estrella", 10, 5);

        // Se guardan las preguntas en el array del objeto cuestionario.
        cuestionario.agregaPregunta(preg01);
        cuestionario.agregaPregunta(preg02);
        cuestionario.agregaPregunta(preg03);
        cuestionario.agregaPregunta(preg04);
        cuestionario.agregaPregunta(preg05);
        cuestionario.agregaPregunta(preg06);
        cuestionario.agregaPregunta(preg07);
        cuestionario.agregaPregunta(preg08);

        // Se inicializan los grupos.
        Grupo grupo1 = new Grupo(1, "Yacares");
        Grupo grupo2 = new Grupo(2, "Jaguaretes");
        Grupo grupo3 = new Grupo(3, "Teros");

        // Se guardan las preguntas en el array del objeto participantes.
        participantes = new Participantes();
        participantes.agregaGrupo(grupo1);
        participantes.agregaGrupo(grupo2);
        participantes.agregaGrupo(grupo3);
    }

    @FXML
    public void handle_btn_nuevo_cuestionario(ActionEvent event) {
        // Vuelve el puntero del objeto cuestionario a la primera pregunta.
        puntero = 0;

        // Enciende el panel del cuestionario.
        pane_cuestionario.setVisible(true);

        // Actualiza los datos en pantalla.
        refrescaPanelCuestionario();
    }

    @FXML
    public void handle_btn_fw(ActionEvent event) {

        // Desplaza el puntero del objeto cuestionario una posicion adelante, si aún no llego al final.
        if (puntero < cuestionario.getPreguntas().size() - 1) {
            puntero++;
            System.out.println(puntero);
        }

        // Actualiza los datos en pantalla.
        refrescaPanelCuestionario();
    }

    @FXML
    private void handle_btn_bw(ActionEvent event) {

        // Desplaza el puntero del objeto cuestionario una posicion atras, si aún no llego al inicio.
        if (puntero > 0) {
            puntero--;
        }
        refrescaPanelCuestionario();
    }

    @FXML
    private void handle_btn_volver1(ActionEvent event) {
        pane_cuestionario.setVisible(false);
    }

    @FXML
    private void handle_btn_nuevo_grupo(ActionEvent event) {
    }

    @FXML
    public void handle_btn_comenzar(ActionEvent event) {
        // Inicializa la variable para que lea el primer boton.
        atiende_grupo = true;

        //Marca la pregunta actual.
        puntero = 0;

        //Habilita la pantalla comenzar.
        pane_comenzar.setVisible(true);

        // Muestra datos en pantalla;
        refrescaPanelComienza();

    }

    @FXML
    private void handle_btn_volver2(ActionEvent event) {
        // Apaga el panel comenzar FALTA SOLO SI SE CUMPLE CIERTA CONDICION.
        pane_comenzar.setVisible(false);
    }

    @FXML
    private void handle_btn_volver3(ActionEvent event) {
        // Apaga el panel ganador
        pane_ganador.setVisible(false);//Apaga panel ganador.
    }

    @FXML
    private void handle_btn_salir(ActionEvent event) {
        // Sale del programa.
        System.exit(0);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public int getPacketSize() {
        return 1;
    }

    @Override
    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
    }

    @Override
    public void serialEvent(SerialPortEvent spe) {

        // Para que no continue cambiando de grupo mientras presionen.
        finalizo = false; // Habiliata la escucha si no llegó el final de la partida.

        System.out.println("Entrooooooo");

        int newData = (int) spe.getReceivedData()[0] & 0xFF;  // lee el byte desde EduTech y lo enmascara con 0xFF
        if (newData != 0x80) {
            text.setText(newData + " " + text.getText()); //muestra en el recuadro cual fue el boton que presiono y si la respuesta esta bien o mal.
        }
        if (!finalizo) {

            // Verifica si está activada la atención a los grupos y si es así verifica quien presiono
            if (atiende_grupo) {

                if (newData == 129) {
                    grupo_presiono = 0;
                    //circulos.get(grupo_presiono).setOpacity(1);
                    atiende_grupo = false;
                }
                if (newData == 130) {
                    grupo_presiono = 1;
                    //circulos.get(grupo_presiono).setOpacity(1);
                    atiende_grupo = false;
                }
                if (newData == 131) {
                    grupo_presiono = 2;
                    //circulos.get(grupo_presiono).setOpacity(1);
                    atiende_grupo = false;
                }
            }

            // Verifica si esta activada la atención a los grupos y si no es así verifica si el grupo respondió bien o mal, según la pulsación del docente.
            if (!atiende_grupo) {
                if (newData == 144) {
                    System.out.println("Grupo que presiono = " + grupo_presiono + " pregunta = " + puntero);
                    participantes.getGrupo(grupo_presiono).sumaPuntos(cuestionario.getPregunta(puntero).getPuntosSuma()); //Suma al grupo que presiono los puntos correspondientes a la pregunta repondida bien.                
                    System.out.println("Puntos del grupo " + grupo_presiono + "ahora: " + participantes.getGrupo(grupo_presiono).getPuntos());
                    atiende_grupo = true;
                    //circulos.get(grupo_presiono).setOpacity(0.5); // Baja la opacidad del circulo del grupo que respondio.
                    siguientePregunta();
                    refrescaPanelComienza();
                }
                if (newData == 160) {
                    System.out.println("Grupo que presiono = " + grupo_presiono + " pregunta = " + puntero);
                    participantes.getGrupo(grupo_presiono).restaPuntos(cuestionario.getPregunta(puntero).getPuntosResta()); //Resta los puntos al grupo que presiono los puntos por responder mal.
                    System.out.println("Puntos del grupo " + grupo_presiono + "ahora: " + participantes.getGrupo(grupo_presiono).getPuntos());
                    atiende_grupo = true;
                    //circulos.get(grupo_presiono).setOpacity(0.5); // Baja la opacidad del circulo del grupo que respondio.
                    siguientePregunta();
                    refrescaPanelComienza();
                }
            }
        }
    }

    public void siguientePregunta() {

        // Incrementa el puntero de cuestionario pero si ya llegó al final, procesa grupos para definir el resultado.
        if (puntero < cuestionario.getPreguntas().size() - 1) {
            puntero++;
        } else {

            pane_ganador.setVisible(true);//Habilita panel ganador.

            //Verifica que grupo gano. 
            if (participantes.getGrupo(0).getPuntos() > participantes.getGrupo(1).getPuntos()) {
                if (participantes.getGrupo(0).getPuntos() > participantes.getGrupo(2).getPuntos()) {
                    grupo_ganador = 0;
                } else {
                    grupo_ganador = 2;
                }
            } else {
                if (participantes.getGrupo(1).getPuntos() > participantes.getGrupo(2).getPuntos()) {
                    grupo_ganador = 1;
                } else {
                    grupo_ganador = 2;
                }
            }

            // Verifica si hubo empate
            empate = 0;
            if (participantes.getGrupo(0).getPuntos() == participantes.getGrupo(1).getPuntos() && participantes.getGrupo(1).getPuntos() == participantes.getGrupo(2).getPuntos()) {
                empate = 4;
            } else {
                if (participantes.getGrupo(0).getPuntos() == participantes.getGrupo(1).getPuntos()) {
                    empate = 1;
                } else {
                    if (participantes.getGrupo(1).getPuntos() == participantes.getGrupo(2).getPuntos()) {
                        empate = 2;
                    } else {
                        if (participantes.getGrupo(0).getPuntos() == participantes.getGrupo(2).getPuntos()) {
                            empate = 3;

                        }
                    }

                }
                // Muestra en pantalla el o los equípos ganadores.

                if (empate == 0) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            label_ganador.setText(participantes.getGrupo(grupo_ganador).getNombre());
                        }
                    });
                    System.out.println("El grupo ganador es = " + (grupo_ganador + 1) + " con " + participantes.getGrupo(grupo_ganador).getPuntos());
                } else {

                    if (empate == 1 && (grupo_ganador == 0 || grupo_ganador == 1)) {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                label_ganador.setText("Empate entre " + participantes.getGrupo(0).getNombre() + " y " + participantes.getGrupo(1).getNombre());
                            }
                        });
                        System.out.println("Empate entre " + participantes.getGrupo(0).getNombre() + " y " + participantes.getGrupo(1).getNombre() + " con " + participantes.getGrupo(grupo_ganador).getPuntos() + " puntos!");
                    } else {
                        if (empate == 2 && (grupo_ganador == 1 || grupo_ganador == 2)) {
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    label_ganador.setText("Empate entre " + participantes.getGrupo(1).getNombre() + " y " + participantes.getGrupo(2).getNombre());
                                }
                            });
                            System.out.println("Empate entre " + participantes.getGrupo(1).getNombre() + " y " + participantes.getGrupo(2).getNombre() + " con " + participantes.getGrupo(grupo_ganador).getPuntos() + " puntos!");
                        }
                        if (empate == 3 && (grupo_ganador == 0 || grupo_ganador == 2)) {
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    label_ganador.setText("Empate entre " + participantes.getGrupo(0).getNombre() + " y " + participantes.getGrupo(2).getNombre());
                                }
                            });
                            System.out.println("Empate entre " + participantes.getGrupo(0).getNombre() + " y " + participantes.getGrupo(2).getNombre() + " con " + participantes.getGrupo(grupo_ganador).getPuntos() + " puntos!");
                        }
                    }

                    //Reinicializa las variables.
                    finalizo = true; //Termino la partida.
                    puntero = 0; //Retorna a la primera pregunta.
                    empate = 0; //No hay empate.
                }
            }
        }
    }
    // Refresca la información en el panel de Comenzar.

    public void refrescaPanelComienza() {

        System.out.println("Grupo que presiono = " + grupo_presiono + "Puntero = " + puntero);
        System.out.println("Hasta aquí sin error");
        String temp1 = String.valueOf(cuestionario.getPregunta(puntero).getIdPregunta());

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                label_numero_pregunta.setText(temp1);
                label_pizarron_verde.setText(cuestionario.getPregunta(puntero).getPregunta());
                label_punto_suma.setText(String.valueOf(cuestionario.getPregunta(puntero).getPuntosSuma())); //Se convierte el int en String para que pueda ser parametro valido.
                label_punto_resta.setText(String.valueOf(cuestionario.getPregunta(puntero).getPuntosResta()));

                label_cont1.setText(String.valueOf(participantes.getGrupo(0).getPuntos()));
                label_cont2.setText(String.valueOf(participantes.getGrupo(1).getPuntos()));
                label_cont3.setText(String.valueOf(participantes.getGrupo(2).getPuntos()));
            }
        });

    }

    // Refresca la información en el panel de Cuestionario.
    public void refrescaPanelCuestionario() {
        textfield_numero_pregunta1.setText(String.valueOf(cuestionario.getPregunta(puntero).getIdPregunta()));
        textfield_pizarron_gris.setText(cuestionario.getPregunta(puntero).getPregunta());
        textfield_punto_suma.setText(String.valueOf(cuestionario.getPregunta(puntero).getPuntosSuma())); //Se convierte el int en String para que pueda ser parametro valido.
        textfield_punto_resta.setText(String.valueOf(cuestionario.getPregunta(puntero).getPuntosResta()));
    }

}
